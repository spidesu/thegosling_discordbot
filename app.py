import discord
import birthday
import schedule
import time


class BotClient(discord.Client):
    command_start = "!"

    async def on_ready(self):
        print('Logged as', self.user)

    async def on_message(self, message):
        if not message.content.startswith(self.command_start):
            return
        if message.author == self.user:
            return
        full_command = message.content[1:]
        command_array = full_command.split(' ')
        command = command_array[0]
        args = command_array[1:]

        cases = {
            'birthday': birthday.process_birthday(message, args),
        }
        response = cases.get(command, None)
        if response is None:
            return


client = BotClient()
client.run('NjcwMzE0NTQ4ODU0Nzg0MDAw.XkpyAA.4vy7CH4EI4pTq4mjbPZEybR5v58')
